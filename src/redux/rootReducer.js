import { combineReducers } from "redux";
// Reducers
import starships from "./reducers/starships";
// import films from "./reducers/films";

export default combineReducers({
  starships
  // films
});