﻿import * as types from '../types';
const apiPath = 'https://swapi.co/api/';

// Get starships
export function fetchStarships() {
  return dispatch => {
    dispatch(fetchStarshipsBegin());
    return fetch(apiPath + 'starships')
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchStarshipsSuccess(json.results));
        return json.results;
      })
      .catch(error => dispatch(fetchStarshipsFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const fetchStarshipsBegin = () => ({
  type: types.FETCH_STARSHIPS_BEGIN,
});

export const fetchStarshipsSuccess = starships => ({
  type: types.FETCH_STARSHIPS_SUCCESS,
  payload: { starships },
});

export const fetchStarshipsFailure = error => ({
  type: types.FETCH_STARSHIPS_FAILURE,
  payload: { error },
});
