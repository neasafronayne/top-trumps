import * as types from '../types';
const apiPath = 'https://api.themoviedb.org/3';
const apiKey = 'api_key=4cb1eeab94f45affe2536f2c684a5c9e';

//Get single Movies
export function fetchMovie(id) {
  return dispatch => {
    dispatch(fetchMovieBegin());
    return fetch(apiPath + '/movie/'+ id + '?' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMovieSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchMovieFailure(error)));
  };
}

//Get single Movie Videos
export function fetchMovieVideos(id) {
  return dispatch => {
    return fetch(apiPath + '/movie/'+ id + '/videos?' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMovieVideosSuccess(json.results));
        return json.results;
      })
      .catch(error => dispatch(fetchMovieFailure(error)));
  };
}

//Get single Movie Cast
export function fetchMovieCast(id) {
  return dispatch => {
    return fetch(apiPath + '/movie/'+ id + '/credits?' + apiKey)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchMovieCastSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchMovieFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const fetchMovieBegin = () => ({
  type: types.FETCH_MOVIES_SINGLE_BEGIN,
});

export const fetchMovieSuccess = movies => ({
  type: types.FETCH_MOVIES_SINGLE_SUCCESS,
  payload: { movies },
});

export const fetchMovieVideosSuccess = videos => ({
  type: types.FETCH_MOVIES_SINGLE_VIDEO_SUCCESS,
  payload: { videos },
});

export const fetchMovieCastSuccess = credits => ({
  type: types.FETCH_MOVIES_SINGLE_CAST_SUCCESS,
  payload: { credits },
});

export const fetchMovieFailure = error => ({
  type: types.FETCH_MOVIES_SINGLE_FAILURE,
  payload: { error },
});
