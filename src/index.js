import React from 'react';
import ReactDOM from 'react-dom';
// Components
import App from './App';
// Store
import { Provider } from 'react-redux';
import store from './redux/store';

ReactDOM.render(
    <Provider store={store} >
        <App />
    </Provider>,
    document.getElementById('root'));
