import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import logo from './logo.svg';
import { connect } from "react-redux";
// Actions
import { fetchStarships } from "./redux/actions/starships";
// Components
import Game from './components/Game/Game';
// Styles
import './style/global.css';

class App extends Component {
  componentDidMount() {
    this.props.dispatch(fetchStarships());
  }

  render() {
    const cards = this.props.data.slice();
    const halfPoint = Math.ceil(cards.length / 2);    
    const userCards = cards.splice(0,halfPoint);
    const compCards = cards.splice(0, halfPoint, 1);

    return (
      <div className="App">
        <Game userCards={userCards} compCards={compCards} />
      </div>
    );

  }
}

App.propTypes = {
    data: PropTypes.array,
    loading: PropTypes.bool,
    error: PropTypes.any,
};

App.defaultProps = {
    data: [],
    loading: false,
    error: '',
};

const mapStateToProps = state => ({
    data: state.starships.data,
    loading: state.starships.loading,
    error: state.starships.error,
});

export default connect(mapStateToProps)(App);
