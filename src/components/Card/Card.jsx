import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Styles
import './Card.css';
// Categories to diplay
const gamesCat = {
    'max_atmosphering_speed': 'Max Speed',
    'cost_in_credits': 'Cost',
    'passengers': 'Passengers',
    'films': 'Films'
};

class Card extends Component {
    handleClick = (cat) => {
        this.props.onClick(cat);
    }
    render() {
        const {
            compCard,
            activeCat,
            name,
            cardCats,
            cardCats: {
                starship_class
            }
        } = this.props;

        return (
            <div className={`card ${compCard && 'computer-card'} ${activeCat && 'active'}`}>
                <h1>{name}</h1>
                <p>{starship_class}</p>
                <ul>
                    { Object.keys(gamesCat).map(cat => (
                        <li 
                            key={cat} 
                            className={activeCat === cat && 'active'} 
                            onClick={ () => { this.handleClick(cat) } }
                        >
                            <strong>{gamesCat[cat]}:</strong> { cat === 'films' ? cardCats[cat].length : cardCats[cat]}
                        </li> 
                    )) }
                </ul>
            </div>
        );
    }
};

Card.propTypes = {
    compCard: PropTypes.bool,
    activeCat: PropTypes.string,
    name: PropTypes.string,
    cardCats: PropTypes.object,
    onClick: PropTypes.func
};

Card.defaultProps = {
    compCard: false,
    activeCat: '',
    name: '',
    cardCats: {},
    onClick: () => {}
};

export default Card;