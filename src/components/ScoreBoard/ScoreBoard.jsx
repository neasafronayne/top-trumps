import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './ScoreBoard.css';


const ScoreBoard = ({ 
    userScore,
    compScore
}) => {
    const total = userScore + compScore;
    const userPercent = (userScore / total) * 100;
    const compPercent = (compScore / total) * 100;

return (
    <div className="scoreboard">
        <div className="title-box">
            <h1>Star wars - Top trump</h1>
        </div>
        <div 
            className="user"
            style={{ height: userPercent + '%' }}
        >
            <div className="name">
                <h3>Hero</h3>
            </div>
            <div className="score">
                <p>{userScore}</p>
            </div>
        </div>
        <div
            className="comp"
            style={{ height: compPercent + '%' }}
        >
            <div className="name">
                <h3>Anti</h3>
            </div>
            <div className="score">
                <p>{compScore}</p>
            </div>
        </div>
    </div>
)};

ScoreBoard.propTypes = {
    userScore: PropTypes.number,
    compScore: PropTypes.number
};

ScoreBoard.defaultProps = {
    userScore: 0,
    compScore: 0
};

export default ScoreBoard;